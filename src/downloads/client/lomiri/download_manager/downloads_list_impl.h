/*
 * Copyright 2014 Canonical Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 3 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef LOMIRI_DOWNLOADMANAGER_CLIENT_DOWNLOAD_LIST_IMPL_H
#define LOMIRI_DOWNLOADMANAGER_CLIENT_DOWNLOAD_LIST_IMPL_H

#include <lomiri/transfers/visibility.h>
#include "downloads_list.h"

namespace Lomiri {

namespace DownloadManager {

class Error;
class Download;

class LOMIRI_TRANSFERS_PRIVATE DownloadsListImpl : public DownloadsList {
    Q_OBJECT

 public:
    DownloadsListImpl(QObject* parent = 0);
    DownloadsListImpl(const QList<QSharedPointer<Download >> downs, QObject* parent = 0);
    DownloadsListImpl(Error* err, QObject* parent = 0);
    virtual ~DownloadsListImpl();

    virtual QList<QSharedPointer<Download> > downloads() const;
    virtual bool isError() const;
    virtual Error* error() const;

 private:
    QList<QSharedPointer<Download> > _downs;
    Error* _lastError = nullptr;
};

}  // Lomiri

}  // DownloadManager

#endif  // LOMIRI_DOWNLOADMANAGER_CLIENT_DOWNLOAD_LIST_IMPL_H
